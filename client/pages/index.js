import Dashboard from '../components/dashboard/dashboard'

export default function Home({report}) {

  //sections: components that will be print on home page
  const sections = [
    {
      id: "features",
      to: "features",
      titleContent: "Reports",
      component: <Dashboard report={report}/>,
      styles: {
        titleColor: "rgb(1,23,42)",
        backgroundColor: "white",
      }
    }]
    return (
      <div
        className="w-100"
        style={{
          minHeight: '90vh',
          backgroundSize: "cover"
        }}
      >
  
  
        {/* Sections from array */}
        <div  className="w-100" tyle={{maxWidth:"1200px"}} >
          {sections.map((section, index) => (
            <div
              key={index}
              id={section.id}
              style={{
                backgroundColor: section.styles.backgroundColor,
              }}
              className="w-100 d-flex flex-column align-items-center px-3 py-5"
            >
              <div className="w-100">
                <div className="text-center">
                  <div 
                    style={{ 
                      color: section.styles.titleColor, 
                      fontWeight:"700", 
                      fontSize: '2.5rem'}}>
                    {section.titleContent}
                  </div>
                </div>
                <div className="text-center">
                  <p style={{ color: section.styles.titleColor }}>____</p>
                </div>
                <div className="w-100">{section.component}</div>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }