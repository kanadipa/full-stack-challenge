import React, { useEffect, useState } from "react";
//import "../public/App.css";
import Head from "next/head";
//import Navbar from "../components/navbar/navbar";
import Footer from "../components/footer/Footer";
import "../styles/globals.css";
import api from "../api/api";
import 'bootstrap/dist/css/bootstrap.css'


function App({ Component, pageProps }) {

/*
   <Navbar //split={split} ovs={ovs} 
       />
  */

 

  return (
    <div className="app">
     <Head>
        <title>Spam Monitoring</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Component 
      {...pageProps} 
      />
      <Footer />
      
    </div>
  );
}

export default App;