import React, {useState, useEffect} from "react";
import api from '../../api/api'
import Router from 'next/router'

export default function Button(props){

    //adding state to change when hovering in and out of element
    const [color,setColor] = useState(props.color)
     return(
            <>
                <button 
                    type='submit'
                    id ={props.id}
                    className='button mx-1 my-2'
                    style={{
                        backgroundColor: color
                    }} 
                    onMouseEnter={() => {
                        setColor(props.color2); 
                    }}
                    onMouseLeave={() => {
                        setColor(props.color);
                    }}
                    onClick={props.onChange}>
                        {props.text}
                </button>
                <style jsx>
                    {`
                    .button{
                        border: solid 2px black;
                        border-radius: 5px;
                        text-align: center;
                        font-weight: 600;
                        max-height: 35px;
                        width: 100px
            
                    }`}
                </style>
            </>
            )
        }