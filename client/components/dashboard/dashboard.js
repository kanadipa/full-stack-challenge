import React, {useState, useEffect} from "react";
import Link from 'next/link'
import Button from "../button/button";

import api from '../../api/api'

export default function Dashboard(){
    //sections: the items from the object that will be print, the text and uri
    const sections=[
        {   
            title: 'Id: ',
            parent_path: 'payload',
            child_path: 'reportId',
        },
        {
            title: 'Type: ',
            parent_path: 'payload',
            child_path: 'reportType',
        },
        {   
            title: 'State: ',
            parent_path: 'state',
            child_path: null
        },
        {   
            title: 'Message: ',
            parent_path: 'payload',
            child_path: 'message'
        }
    ]
    //buttons: charachteristics from the buttons and the input value that is sent to express
    const buttons =[
        {
            title: 'Block',
            input_text: 'BLOCKED',
            color: '#e86161'
        },
        {
            title: 'Resolve',
            input_text: 'RESOLVED',
            color: '#1ed0da'
        }
    ]
    
  
    const [data, setData] = useState([]);// adding state that saves current data
    const [id,setId] = useState(null);// adding state that saves report id 
    const [state,setState] = useState(null);// adding state that saves new ticket state
   
    //function that calls axios api - get
    const getData = async () => {
        const res1 = await api.getReports({ timeout: 1000 * 30 });
        setData(res1.data.data);
      };

      
    useEffect(() => {
        getData()
        
        //function that calls axios api - put
        async function submitData(){
            try {
                if (state){
                    await api.updateReport(
                        id, {state: state}, 
                        {timeout: 1000 * 40,} , )
                    }
                } catch (error) {
                console.log(error)
                alert(error)
            }
        }
        if (state) {
            console.log(state);
            submitData();
            setState(null);
            
        }
    }, [state])

    
    return (

    
    <div >
        {data.map((data, index)=>{
              const unique = data.elements.filter((obj, pos, arr) => {
                    return arr.map(mapObj => 
                        mapObj['payload']['reportId']).indexOf(obj['payload']['reportId']) === pos;
                });
            return(
            <div  key={index} >
                {unique.map((element, index)=>(
                    element['state'] !== 'BLOCKED' &&
                    element['state'] !== 'RESOLVED'
                    ?
                    <div className='container' key={index}> 
                        {/* Sections from array */}
                        <div className='container-text py-1'>
                            {sections.map((item, i)=>(
                                    <div  key={i} className=' mx-3'>
                                    <span className='item'>
                                            {item.title}  
                                            </span>
                                            <span>
                                            {
                                                item.child_path!==null?
                                                    element[item.parent_path][item.child_path]:
                                                    element[item.parent_path]
                                            }
                                            {
                                                item.child_path === 'message' && 
                                                element[item.parent_path][item.child_path] == null
                                            ?
                                                "Some message...":
                                                null}
                                        </span>
                                    </div>
                                ))}
                                <div className='item w-100 px-3 '>
                                    <Link href="http://linkedin.com/in/karen-diazpaucar">
                                        <a 
                                            className='link' 
                                            target="_blank" 
                                            rel="noreferrer">Details</a>
                                    </Link>
                                </div>
                    </div>
                        {/* Buttons from array */}
                        <div  className='container-buttons'>
                            {buttons.map((button, i2)=>(
                                <Button
                                    key={i2}
                                    color='#ececec'
                                    color2={button.color}
                                    text={button.title}
                                    onChange={function onChange(){
                                        setState(button.input_text);
                                        setId(element['payload']['reportId'])
                                        getData()
                                    }}
                                    
                                />
                                    
                            ))}
                        </div>
                    </div>
                   :index===0?
                   <div className='container px-5 py-5'>
                       <h1 className='text-center'>
                           You have successfully cleaned up all the spam!</h1>
                    </div>
                    :null                
                   ))}
                </div>  
            )})}
        <style jsx>{`
            .container{
                padding: 0.25rem;
                display: flex;
                max-width: 600px;   
                border: 1px solid black;
                justify-content: space-evenly;
                font-weight: 300
            }
            .container-text{
            display: flex;
                flex: 100%;
                flex-wrap: wrap;
                justify-content: space-evenly
            }
            .container-text > * {
                flex-grow: 1;
            }
            .container-buttons{
                display: flex;
                flex-wrap: wrap;
                justify-content: flex-end

            }
            .container-buttons > * {
                flex-grow: 1;
            }
            .item{
                font-weight: 600
            }
            .link{
                color: #0fa2ab
            }
            }`}
        </style>
    </div>
    )}