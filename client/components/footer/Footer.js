import React from 'react'
export default function Footer(props){
    return(
        <div className="footer">
            <div className="text-center">
            © KANADIPA, 2021. 
            </div>
            <style jsx>{`
                .footer {
                
                width:100%;
                bottom: 0;
                }
            `}</style>
        </div>
    ) 
}
