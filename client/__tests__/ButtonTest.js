import React from 'react';
import Button from '../components/button/button';
import {render, fireEvent, cleanup} from '@testing-library/react';
import Dashboard from '../components/dashboard/dashboard';

afterEach(cleanup)

//test checks if the state in item can be changed
it('button click changes props', () => {
    const onChange = jest.fn();
    const { getByText } = render(<Dashboard>
                                    <Button onChange={onChange}/>
                                </Dashboard>);
    const handleClick = jest.spyOn(React, "useState");
    handleClick.mockImplementation(state => [state, setState]);
    expect(onChange).toBeTruthy();
})