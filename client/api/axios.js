const _axios = require('axios')
const axiosRetry = require('axios-retry')
const axios = _axios.create({
    baseURL: process.env.NEXT_PUBLIC_BACKEND_URL,
  });
                

// https://github.com/softonic/axios-retry/issues/87
const retryDelay = (retryNumber = 0) => {
    const seconds = Math.pow(3, retryNumber) * 1000;
    const randomMs = 1000 * Math.random();
    return seconds + randomMs;
};

axiosRetry(axios, {
    retries: 3,
    retryDelay,
    // retry on Network Error & 5xx responses
    retryCondition: axiosRetry.isRetryableError,
});

module.exports = axios;