import api from './axios'

export const updateReport = (id, payload) => api.put(`/reports/${id}`, payload)
export const getReports = () => api.get(`/reports`)

const apis = {
    getReports,
    updateReport,
}

export default apis
