#  Full-Stack Challenge

- [Architecture](#architecture-and-frameworks-used)
- [Usage](#usage)
- [Testing](#testing)


## Architecture and Frameworks used

#### *NextJS* for FrontEnd
- Server Side Rendering (SSR)
- Automatic code splitting
- Fast Refresh

The `client` folder includes the respective UI. The server is mounted separately from the back-end for scalability purposes. The server is the default server by Next. An additional configuration to the server can be adapted for further needs.

#### *Express and NodeJS* for Middleware
- Enables middleware functions
- Simplifies development
- Routing included

The `server` folder includes the respective middle and back-end configurations. The server is mounted separately from the front-end for scalability purposes.

#### *MongoDB* and Mongoose for Database
- High degree of flexibility
- Easily enforced schema
- Minimizes risk of unwanted changes


<img src="client/public/architecture.png" alt="architecture" width="600"/>

## Usage
To run the 3 components, NextJS, Express Server, and MongoDB, they can be run on three different terminal paths. 
### Setting up the DB
```
brew tap mongodb/brew
brew install mongodb-community@4.4
mkdir data
mongod --dbpath=./data
mongoimport --db reports --collection spam-report --drop --file ./reports.json --jsonArray
```

### Setting up the App
```
cd server
touch .env 
```
Open the .env file and write the following variables:
- NODE_ENV=development
- PORT=
- MONGODB_CONNECTION_STRING=mongodb://localhost:27017/reports
- SHOW_EXAMPLE_RESPONSE=FALSE

```
npm install
npm start
```

### Setting up the Client
```
cd client
touch .env
```
Open the .env file and write the following variables:
- NEXT_PUBLIC_BACKEND_URL=http://localhost:8080/api
```
npm install
npm run build
npm start
```


## Testing

For testing, the framework *Jest* has been implemented both for front-end and back-end. The configurations for the tests are done separetely. 

For NextJS/React, the packages`jest`, `babel-jest`, and `react-test-renderer` are in charge of the unit tests. Due to the simplicity of the app, and changing requirements, simple tests have been conducted. When further development of components, additional tests will be integrated. 

As for testing the database, the packages `@shelf/jest-mongodb`, `babel-jest`, and `jest` are in charge to test the queries to the database by mocking the connection. 
```
npm run test
```


I look forward to meeting the team! It was a fun challenge. 

