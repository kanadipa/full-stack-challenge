const express = require('express')
const server = express()
const dev = process.env.NODE_ENV !== 'production'
if (dev) { require('dotenv').config() }
const host = process.env.IP  || '0.0.0.0';
const port = process.env.PORT || 8080;
const mongoose = require('mongoose')
const cors = require('cors')
const logger = require("morgan");

//Import API routes
const reportsRouter = require('./routes/reports-routes')

// Connect to MongoDB with Mongoose
mongoose
    .connect(process.env.MONGODB_CONNECTION_STRING, 
        {
         useNewUrlParser: true, 
         useFindAndModify: false, 
         useUnifiedTopology: true
        } )
    .catch(err => {
        console.log(err)
        console.log('Error connecting to database')});
const db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
    console.log('Connected to mongodb')
});

//Middleware Configuration
server.use(express.json()) //Used to parse JSON bodies
server.use(express.urlencoded({limit: '50mb', extended: true}))////Parse URL-encoded bodies
server.use(cors())// Enbale CORS
server.use(logger('dev'));//Configure Express middleware to log HTTP requests 

//Use API routes in the App
server.use('/api', reportsRouter, )
server.get('/', (req, res) => {
   res.json({
      message: 'Server app running',
      baseUrl: '/api/reports',
      endpoints: [
        {method: 'GET', path: '/api/reports', description: 'Describes all available endpoints'},
        {method: 'PUT', path: '/api/reports/:reportId', description: 'Updates a ticket based on an ID'},
       ]
    })
  });

// Launch app to the specified port
server.listen(port, host, err => {
        if (err) throw err
        console.log(`Server is running on port ${port} with ip ${host}`)
    })
