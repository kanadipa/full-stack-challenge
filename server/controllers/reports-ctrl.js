const { any } = require('prop-types')
const reportsModel = require('../models/reports-models')

//retrieve list of all tickets in report
getReports = async (req, res) => {
    await reportsModel.find({}, (err, rep) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!rep) {
            return res
                .status(404)
                .json({ success: false, error: `Ticket not found` })
        }
        return res.status(200).json({ success: true, data: rep })
    })
    .catch(err => {
        res.status(422).send(err.errors);
    });
}   
//update item of ticket list in report
updateReport = async (req, res) => {
     await reportsModel.updateOne({ 
         "elements.payload.reportId": req.params.id },
         {"elements.$.state":req.body.state},(err, rep) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!rep.nModified) {
            return res.json({ success: false, message: 'No Item was updated' })}
        return res.status(201).json({success: true, ItemsModified: rep.nModified, message:'Ticket was succesfully updated' })
    })
    .catch(err => {
        res.status(422).send(err.errors);
    });
}   

module.exports = {
    getReports,
    updateReport
}