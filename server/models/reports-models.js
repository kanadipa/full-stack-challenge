const mongoose = require('mongoose')
const Schema = mongoose.Schema

//model of object in reports.json
const reportsModel = new Schema(
    {   
        size: Number,
        nextOffset: String, 
        elements: [{
            id: String,
            source: String,
            sourceIdentityId: String,
            reference: {
                referenceId: String,
                referenceType: String,
            },
            state: String,
            payload: {
                source: String,
                reportType: String,
                message: String,
                reportId: String,
                referenceResourceId: String,
                referenceResourceType: String
            },
            
        }]
    }
)

module.exports = mongoose.model( 'reports', reportsModel, 'spam-report')