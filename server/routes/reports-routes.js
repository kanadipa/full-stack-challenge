const express = require('express')
const ReportsCtrl = require('../controllers/reports-ctrl')

//routes to send requests to db
const router = express.Router()

router.put('/reports/:id', ReportsCtrl.updateReport)
router.get('/reports', ReportsCtrl.getReports)

module.exports = router