const mongoose = require('mongoose');

const dbHandler = require('./db-handler');
const ReportsController = require('../controllers/reports-ctrl');

//Connect to a new in-memory database before running any tests.
beforeAll(async () => await dbHandler.connect());

// Clear all test data after every test.

afterEach(async () => await dbHandler.clearDatabase());

//Remove and close the db and server.
afterAll(async () => await dbHandler.closeDatabase());

//Report test suite.
describe('report ', () => {

    /**
     * Tests that the list of reports can be retrieved through the controller 
     * without throwing any errors.
     */
    it('can retrieve list of items correctly', async () => {
        expect(async () => 
            await ReportsController.getReports())
    });
});