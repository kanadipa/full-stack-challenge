const mongoose = require('mongoose');

const dbHandler = require('./db-handler');
const ReportsController = require('../controllers/reports-ctrl');

//Connect to a new in-memory database before running any tests.
beforeAll(async () => await dbHandler.connect());

//Clear all test data after every test.
afterEach(async () => await dbHandler.clearDatabase());

// Remove and close the db and server.
afterAll(async () => await dbHandler.closeDatabase());

// Report test suite.
describe('report ', () => {

    /**
     * Tests that an item of the list of reports can be updated through the controller 
     * without throwing any errors.
     */
    it('can be updated correctly', async () => {
        expect(async () => 
            await ReportsController.updateReport(
                reportComplete.payload.reportId, 
                {"elements.$.state":"Blocked"}))
    });
});
/**
 * Complete reportComplete example.
 */

const reportComplete = {
    id: "0103e005-b762-485f-8f7e-722019d4f302",
    source: "REPORT",
    sourceIdentityId: "6750b4d5-4cb5-45f0-8b60-61be2072cce2",
    reference: {
        referenceId: "6706b3ba-bf36-4ad4-9b9d-4ebf4f4e2429",
        referenceType: "REPORT"
    },
    state: "OPEN",
    payload: {
        source: "REPORT",
        reportType: "SPAM",
        message: null,
        reportId: "6706b3ba-bf36-4ad4-9b9d-4ebf4f4e2429",
        referenceResourceId: "a03411ce-0197-49a2-86d4-55e06aa52e79",
        referenceResourceType: "REPLY"
    }}